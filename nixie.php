<?php

error_reporting(E_ALL);

$DISTANCE_LIMIT = 15;
$STATION_LAT = 39.08729346743942;
$STATION_LONG = -76.70605256424655;

$trainDisplay = [];

marc();
amtrak();
sortTrains();
filterTrains();
displayTrains();

function marc() {
	global $trainDisplay;

	$data = fetch("https://www.mta.maryland.gov/marc-tracker/fetchvehicles");
	$trains = $data['vehicleArr'];

	if (isset($trains['trains'])) {
		$trains = $trains['trains'];
	} else {
		return;
	}


	foreach ($trains as $train) {
		
		$route = $train['route_name'];
		
		if ($route != "PENN") {
			continue;
		}

		$lat = $train['lat'];
		$long = $train['lon'];

		$dest = $train['trip_headsign'];
		$num = explode(' ', $train['trip_name'])[1];
		$direction = $train['trip_headsign'] == 'WASHINGTON' ? 'South' : 'North';

		$trainDisplay[] = (object)[
			'name'      => 'Marc Penn',
			'direction' => $direction,
			'distance'  => determineDistance($lat, $long),
			'hasPassed' => determineIfPassed($lat, $direction),
			'number'    => explode(' ', $train['trip_name'])[1],
		];
	}
}

function amtrak() {
	global $trainDisplay;

	$localTrains = ["Northeast Regional", "Acela Express"];
	
	$data = fetch("https://asm.transitdocs.com/api/asm.php");

	foreach ($data['trains'] as $train) {
		$name = $train['name'];

		if (!in_array($name, $localTrains)) {
			continue;
		}

		$lat  = $train['rt']['coords'][1];
		$long = $train['rt']['coords'][0];

		$dir = $train['rt']['direction'];
		$dir = in_array($dir, ['N','NE','E']) ? 'North' : 'South';

		$trainDisplay[] = (object)[
			'name'			=> $train['name'],
			'number'    => $train['number'],
			'direction' => $dir,
			'speed'     => $train['rt']['speed'],
			'hasPassed' => determineIfPassed($lat, $dir),
			'distance'	=> determineDistance($lat, $long)
		];
	}
}

function determineIfPassed($lat, $direction) {
	global $STATION_LAT;

	$latDiff = $STATION_LAT - $lat;	
	$hasPassed = ($latDiff > 0 && $direction == 'South') || ($latDiff < 0 && $direction == 'North');

	return $hasPassed;
}

function fetch($url) {
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); # don't print on curl_exec

	$result = curl_exec($ch);
	return json_decode($result, true);
}

function determineDistance($lat, $long) {
	global $STATION_LAT, $STATION_LONG;

	$theta = $STATION_LONG - $long;

	$dist = sin(deg2rad($STATION_LAT)) * sin(deg2rad($lat));
	$dist += cos(deg2rad($STATION_LAT)) * cos(deg2rad($lat)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);

	$miles = $dist * 60 * 1.1515;

  return round($miles, 2); # round to 2 decimals
}

function sortTrains() {
	global $trainDisplay;

	usort($trainDisplay, 
		function($a, $b) {
			return $a->distance > $b->distance;
		});
}

function filterTrains() {
	global $trainDisplay;

	$trainDisplay = array_filter($trainDisplay, 
		function($t) {
			global $DISTANCE_LIMIT; //has to be here
			return $t->distance < $DISTANCE_LIMIT;
		});
}

function displayTrains() {
	global $trainDisplay, $DISTANCE_LIMIT;

	print "<title>WhistleBoard</title>";

	printTable('Arriving', array_filter($trainDisplay, 
		function($t) { return !$t->hasPassed; }
	));

	printTable('Passed Through', array_filter($trainDisplay, 
		function($t) { return $t->hasPassed; }
	));
}

function printTable($title, $trains) {
	global $DISTANCE_LIMIT;

	print "<h2>$title</h2>";

	if (!count($trains)) {
		$title = strtolower($title);
		print "No trains $title within $DISTANCE_LIMIT miles.";
		return;
	}

	print "<table cellpadding=10 cellspacing=0>";

	print "
	<tr>
	<th>#</th>
	<th>Line</th>
	<th>Direction</th>
	<th>Speed</th>
	<th>Distance</th>
	</tr>
	";

	foreach ($trains as $t) {
		$speed = array_key_exists('speed', $t) ? "$t->speed mph" : '';

		print "<tr>";
		print "<td>#$t->number</td>";
		print "<td>$t->name</td>";
		print "<td>$t->direction</td>";
		print "<td>$speed</td>";
		print "<td>$t->distance miles</td>";
		print "</tr>";
	}

	print "</table>";
}

?>