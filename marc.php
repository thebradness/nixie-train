<?php

# php -f test.php

$STATION_LAT = 39.08729346743942;
$STATION_LONG = -76.70605256424655;

$result;

print "\n\n";

getTrains();
// getFakeTrains();
processTrains();

print "\n\n";


function getFakeTrains() {

	global $result;

	$result = json_decode(json_encode('{
		"vehicleArr": {
			"trains": [{
				"entity_id": "94081",
				"lat": 38.94949,
				"lon": -76.8708038,
				"trip_id": "2566974",
				"trip_name": "Train 694",
				"trip_headsign": "MARTIN AIRPORT",
				"destination": "MARTIN AIRPORT",
				"route_name": "PENN",
				"delay": 194
			}]
		}}'), true);

	// var_dump($result);
}

function getTrains() {
	global $result;

	echo "connecting...";

	$url = "https://www.mta.maryland.gov/marc-tracker/fetchvehicles";
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); # don't print on curl_exec

	$result = curl_exec($ch);

	// var_dump($result);
}

function processTrains() {
	global $result;

	$data = json_decode($result, true);
	$trains = $data['vehicleArr']['trains'];

	// var_dump($trains);
	if ($trains == NULL) {
		print "\n\nNO TRAINS";
		return;
	}

	foreach ($trains as $train) {
		$id = $train['entity_id'];
		$tripId = $train['trip_id'];

		$lat = $train['lat'];
		$long = $train['lon'];
		$dist = determineDistance($lat, $long);

		$dest = $train['destination'];
		$route = $train['route_name'];
		$trainNum = $train['trip_name'];

		if ($route != "PENN") {
			continue;
		}

		print "\n\n";
		print "$route $trainNum ($dest)";
		// print "\n\tentity_id: $id, trip_id: $tripId";
		print "\n\t$lat, $long";
		print "\n\tDistance: $dist miles";
	}
}

function determineDistance($lat, $long) {
	global $STATION_LAT, $STATION_LONG;

	$theta = $STATION_LONG - $long;

    $dist = sin(deg2rad($STATION_LAT)) * sin(deg2rad($lat));
    $dist += cos(deg2rad($STATION_LAT)) * cos(deg2rad($lat)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);

    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    return round($miles, 2); # round to 2 decimals
}

?>