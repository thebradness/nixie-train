
<?php

$url = "https://asm.transitdocs.com/api/asm.php";
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); # don't print on curl_exec

$result = curl_exec($ch);
$data = json_decode($result, true);

foreach ($data['trains'] as $train) {
	$name = $train['name'];

	// if ($name != "Northeast Regional" && $name != "Acela Express") {
	// 	continue;
	// }

	$num = $train['number'];
	$lat = $train['rt']['coords'][1];
	$long = $train['rt']['coords'][0];

	print "$name (#$num) $lat, $long" . "\n";
}
?>