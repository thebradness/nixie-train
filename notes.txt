https://www.amtrak.com/services/maps.trainlocation.html

:::CONSOLE:::

Object.values(resolver.trains).forEach(tl => tl.forEach(t => {
    props = t.properties;
    lat = props.lat;
    long = props.lon;

    if (lat && long)
        console.log(props.RouteName, lat, ",", long);
}))

console variables
__$$_jmd - public key 
- "69af143c-e8cf-47f8-bf09-fc1f61e5cc33"

This gives me an array with a new key and a timestamp
- it's the last 88 chars of the data from the request
__$_s1._$_dcrt("HqUbpK/ZkGUT+eHVzafp0z/AgVVr7OvkxRhY8WZtT8jZABaLuyEppIebqKP24BiiDBKwtB90NK6tjEcTcFApow==", "69af143c-e8cf-47f8-bf09-fc1f61e5cc33").split('|')

https://maps.amtrak.com/services/MapDataService/stations/nationalRoute?routeName=
- this might have something interesting
- it doesn't, it's just the lat/longs to draw the route lines on the map

__$_s1._$_dcrt("HqUbpK/ZkGUT+eHVzafp0z/AgVVr7OvkxRhY8WZtT8jZABaLuyEppIebqKP24BiiDBKwtB90NK6tjEcTcFApow==", "69af143c-e8cf-47f8-bf09-fc1f61e5cc33")
"f2ffd63d-9ac9-4c40-9999-7123982a9fb1|2021-04-19T10:11:51.000Z"

With a php script I should be able to decrypt:
"HqUbpK/ZkGUT+eHVzafp0z/AgVVr7OvkxRhY8WZtT8jZABaLuyEppIebqKP24BiiDBKwtB90NK6tjEcTcFApow=="
with this key:
"69af143c-e8cf-47f8-bf09-fc1f61e5cc33"
And get this key / timestamp
"f2ffd63d-9ac9-4c40-9999-7123982a9fb1|2021-04-19T10:11:51.000Z"

Helper::dcrt 
- AS::Base64.parse (converts the 88 chars to an size 16 array of 9 digit numbers)
- 